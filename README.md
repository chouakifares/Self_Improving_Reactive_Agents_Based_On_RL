# Self Improving Reactive Agents Based On RL



## Getting started

This is an attempt at recreating the results of the article "Self-Improving Reactive Agents Based On
Reinforcement Learning, Planning and Teaching" by LONG-JI LIN.

## How To Run ?
Start by installing the necessary requirements using the command
`pip install -r requirements.txt`
Then you'll be able to run the experiments and generate your own data using the command
`python3 test.py`
You can modify the algorithm used to train your agent by modifying the value of the variable `learning_algo`. You can also enable or disable the replay function by modifying the value of the variable `replay`.
