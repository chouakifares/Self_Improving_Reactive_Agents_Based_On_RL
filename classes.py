import pygame as pg
import numpy as np
import sys
import torch
from torch import nn
from collections import deque
from matplotlib import pyplot as plt
import seaborn as sns


class AdaptedSigmoid(nn.Module):
    """
        implementation of the sigmoid function described in the article
    """
    def __init__(self):
        super().__init__() # init the base class

    def forward(self, input):
        return 2*(torch.sigmoid(input)-0.5)

class QNetwork(nn.Module):
    """
        Neural network used in the paper implmented in pytorch
    """
    def __init__(self,input_layer=145, hidden_layer=512, output_layer=1):
        super(QNetwork, self).__init__()
        adapted_sigmoid = AdaptedSigmoid()
        self.network = nn.Sequential(
            nn.Linear(input_layer, hidden_layer),
            adapted_sigmoid,
            nn.Linear(hidden_layer, output_layer),
            nn.Identity(),
        ).to(torch.device('cpu'))

    def forward(self, x):
        return self.network(x.float())

def init_weights(m):
        if isinstance(m, nn.Linear):
            torch.nn.init.uniform_(m.weight, a=0, b = 0.1)



class Sensor:

    def __init__(self, relative_position, activation_cells, activation_value):
        """
            Generic class implementing sensor type
            params:
                relative_position(int, int): the relative position of
                    this sensor with respect to the agent
                activation_cells[(int, int)]: list of cells that activate th sensor,
                    the cells are given with their relative position with respect to the
                    position of the sensor
        """
        self.relative_position = relative_position
        self.activation_cells = activation_cells
        self.activated = False
        self.activation_value = activation_value

class YSensor(Sensor):
    def __init__(self,relative_position, activation_value):
        super().__init__(
            relative_position,
            [
                (0,0),(0,-1),(0,-2),(0,1),(0,2),(1,0),(1,1),(1,-1),
                (-1,0),(-1,1),(-1,-1),(2,0),(-2,0)
            ],
            activation_value
        )

class OSensor(Sensor):
    def __init__(self,relative_position,activation_value):
        super().__init__(
            relative_position,
            [(0,0),(0,-1),(0,1),(1,0),(-1,0),(1,1),(1,-1),(-1,1),(-1,-1)],
            activation_value
        )


class XSensor(Sensor):
    def __init__(self,relative_position, activation_value):
        super().__init__(
            relative_position,
            [(0,0),(0,-1),(0,1),(1,0),(-1,0)],
            activation_value
        )

class ObstacleSensor(Sensor):
    def __init__(self,relative_position, activation_value):
        super().__init__(relative_position,[(0,0)], activation_value)

class Agent:
    """
        Learning agent class
        params:
            obstacle_sensors([obstacle_sensors]): list of the obstacle sensors of the agent
            enemy_sensors([enemy_sensors]): list of the enemy sensors of the agent
            food_sensors([food_sensors]): list of the food sensors of the agent
            replay(bool): boolean used to specify wether to use experience replay or not during the learning phase
            buffer_size(number): size of the buffer used for saving experience
            mini_batch_size (number): size of the mini batch used for training qnetwork
            hidden_layer_size(number): number of units (neurones) in the hidden layer of the QNetwork
            gamma(number): discount factor used in the TD-learning formula
            alpha(number): learning rate of the optimizer
    """
    def __init__(
        self,
        obstacle_sensors,
        enemy_sensors,
        food_sensors,
        replay = False,
        buffer_size = 1024,
        mini_batch_size = 64,
        hidden_layer_size = 30,
        gamma = 0.9,
        alpha = 0.001):
        self.action_set = []
        self.obstacle_sensor_list = []
        self.enemy_sensor_list = []
        self.food_sensor_list = []
        self.replay = replay
        self.buffer_size = buffer_size
        #nb of experience quadruplets used to calculate the gradient at each learning step
        self.mini_batch_size = 1
        if(replay):
            self.mini_batch_size = mini_batch_size
            self.memory = deque(maxlen = buffer_size)
        else:
            self.memory = deque(maxlen = 1)
        #initial energy level of the agent
        self.energy_level = 40
        #initial previous action took by the agent (None since at the beginning of the episode agent didn't perform any action)
        self.previous_action = None
        #wether or not the agent hit an obstacle at the previous action
        self.hit_obstacle = False
        self.gamma = gamma
        self.alpha = alpha
        self.qnetwork = QNetwork(145, hidden_layer_size, 1)
        #qtable in case you're crazy enough to test it (this can work if you only put 1 food in the env and take everything else away)
        self.qtable= {}
        self.qnetwork.apply(init_weights)
        self.target = QNetwork(145, hidden_layer_size, 1)
        self.target.load_state_dict(self.qnetwork.state_dict())
        #nb of learning steps (used for synchronising target and pred network of DQN)
        self.nb_iter = 0
        self.optimizer = torch.optim.Adam(self.qnetwork.parameters(), lr=self.alpha)
        self.__create_sensor__lists(
            obstacle_sensors,
            enemy_sensors,
            food_sensors
        )


    def __create_sensor__lists(self,obstacle_sensors,enemy_sensors,food_sensors):
        """
            function that initializes the sensors of the agent from a list of sensors given as params
            params:
                obstacle_sensors([obstacle_sensors]): list of obstacle sensors of the agent
                enemy_sensors([enemy_sensors]): list of enemy sensors of the agent
                food_sensors([food_sensors]): list of food sensors of the agent
        """
        #creating obstacle sensors
        for (sensor_type, pos, activation_value) in obstacle_sensors:
            self.obstacle_sensor_list.append(sensor_type(pos, activation_value))

        for (sensor_type, pos, activation_value) in enemy_sensors:
            self.enemy_sensor_list.append(sensor_type(pos, activation_value))

        for (sensor_type, pos, activation_value) in food_sensors:
            self.food_sensor_list.append(sensor_type(pos, activation_value))

    def select_action(self, t):
        """
        function that chooses the action of the agent using a softmax policy with
        respect to the values of the actions predicted by the neural network
        β = 1/t
        params:
            t(number)
        returns (str): action to perform by the agent can be one of the elements of
        this list ["N", "E", "S", "W"]
        """
        values = []
        self.qnetwork.eval()
        with torch.no_grad():
            for a in self.action_set:
                x = self.get_observation_state(a)
                values.append(self.qnetwork(torch.from_numpy(x)).detach().numpy().item())
        self.qnetwork.train()
        if(t):
            max_ = max(values)
            K = sum([np.exp((i - max_)/t) for i in values])

            proba = np.array([np.exp((i-max_)/t)/K for i in values])
            return np.random.choice(self.action_set, p = proba)
        else:
            return self.action_set[values.index(max(values))]

    def select_action_table(self, t):
        """
        function that chooses the action of the agent using a softmax policy with
        respect to the values of the actions predicted by the qtable
        β = 1/t
        params:
            t(number)
        returns (str): action to perform by the agent can be one of the elements of
        this list ["N", "E", "S", "W"]

        """
        values = []
        for a in self.action_set:
            x = [str(int(i)) for i in self.get_observation_state(a)]
            s = "".join(x)
            if(s not in self.qtable.keys()):
                self.qtable[s]= 0


            values.append(self.qtable[s])
        if(t):
            max_ = max(values)
            K = sum([np.exp((i-max_)/t) for i in values])
            proba = np.array([np.exp((i-max_)/t)/K for i in values])
            return np.random.choice(self.action_set, p = proba)
        else:
            return self.action_set[values.index(max(values))]


    def __sensor_to_matrix__(self, sensor_list, mat_size):
        """
            function that creates a matrix from a list of sensors of the agent
                using the relative position of the sensor with respect to the position
                of the agent
                (this function is used to rotate the observations
                    and create the input vector of the qnetwork)
            params:
                sensors_list ([Sensor]): list of one type of sensors of the agent
                mat_size(int): size of the matrix to create
                    (
                        9 for the obstacles sensors,
                        13 for the enemy sensors,
                        21 for the food sensors,
                    )
            returns 2d-array
        """
        matrix = np.zeros((mat_size,mat_size), dtype=int)
        for s in sensor_list:
            if s.activated:
                i,j = s.relative_position
                matrix[j+ mat_size//2  ,i+ mat_size//2] = 1
        return matrix

    def __matrix_to_vector__(self,matrix, sensor_type):
        """
            function that creates a vector from a matrix that represent
            the sensors of the agent, it gathers the values of the
            points of interest (points of the matrix that correspond the positions
            of the sensors in the sensor list) and puts them in the vectos
                (this function is used to rotate the observations
                    and create the input vector of the qnetwork)
            params:
                sensors_list ([Sensor]): list of one type of sensors of the agent
                sensor_type(str): either "obstacle", "food" or "enemy"
            returns 1d-array
        """
        vector = []
        if sensor_type == "obstacle":
            sensor_list = self.obstacle_sensor_list
        elif sensor_type == "food":
            sensor_list = self.food_sensor_list
        elif sensor_type == "enemy":
            sensor_list = self.enemy_sensor_list

        mat_size = matrix.shape[0]
        for s in sensor_list:
            i,j = s.relative_position
            vector.append(matrix[j+ mat_size//2, i+ mat_size//2])
        return vector


    def tabular_learn(self):
        """
            function that performs tabular qlearning, be careful when using this
            since the space complexity of this code is in O(2^145) if you use
            all the sensors used in the article, I personnally coded this to
            check whether the sensors were working properly and the rotations were
            done correctly, I also used it to check if my TD-learning works but i restricted
            the env to a blank grid containing only one piece of food.
        """
        if(self.replay):
            if(len(self.memory)< self.buffer_size-1):
                return

        mini_batch = np.random.choice(self.memory, size = self.mini_batch_size, replace=False)
        y, y_hat = torch.zeros((self.mini_batch_size,)),torch.zeros((self.mini_batch_size,))
        batch_s_t = [mini_batch[i]["s_t"] for i in range(len(mini_batch))]
        batch_s_t1 = [mini_batch[i]["s_t+1"] for i in range(len(mini_batch))]
        batch_r_t = [mini_batch[i]["r_t"] for i in range(len(mini_batch))]
        batch_done = [mini_batch[i]["done"] for i in range(len(mini_batch))]
        for i in range(self.mini_batch_size):
            tmp = [str(int(j)) for j in batch_s_t[i]]
            s = "".join(tmp)

            next_s = []
            for k in batch_s_t1[i].values():
                tmp = [str(int(j)) for j in k]
                next_s.append("".join(tmp))
            if(s not in self.qtable.keys()):
                self.qtable[s]= 0
            for n in next_s:
                if(n not in self.qtable.keys()):
                    self.qtable[n] = 0
            if(batch_done[i]):
                self.qtable[s] += self.alpha *(batch_r_t[i] - self.qtable[s])
            else:
                self.qtable[s] += self.alpha * (batch_r_t[i] + max([self.qtable[n] for n in next_s]) - self.qtable[s])

    def DQNlearn(self):
        """
            function that performs DQN learning
        """
        if(self.replay):
            if(len(self.memory)< self.mini_batch_size):
                size = len(self.memory)
            else:
                size = self.mini_batch_size
        mini_batch = np.random.choice(self.memory, size = self.mini_batch_size, replace=False)

        y, y_hat = torch.zeros((self.mini_batch_size,)),torch.zeros((self.mini_batch_size,))
        batch_s_t = torch.stack([torch.Tensor(mini_batch[i]["s_t"]) for i in range(len(mini_batch))])

        for i in range(len(mini_batch)):
            self.qnetwork.train()
            y_hat[i] = self.qnetwork(torch.tensor(list(mini_batch[i]["s_t"])))
            self.qnetwork.eval()
            if not mini_batch[i]["done"]:
                y[i] = (mini_batch[i]["r_t"]
                    + self.gamma * self.target(torch.tensor([list(i) for i in mini_batch[i]["s_t+1"].values()])).max())

            else:
                y[i] = mini_batch[i]["r_t"]

        self.qnetwork.train()
        loss = torch.nn.MSELoss()(y , y_hat)
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
        self.nb_iter+=1
        if self.nb_iter%1000==0:
            self.target.load_state_dict(self.qnetwork.state_dict())

    def qconlearn(self):
        """
            function that performs QCON learning (algorithm described in the article)
        """
        if(self.replay):
            if(len(self.memory)< self.mini_batch_size):
                size = len(self.memory)
            else:
                size = self.mini_batch_size
        else:
            size = 1

        mini_batch = np.random.choice(self.memory, size = size, replace=False)

        y, y_hat = torch.zeros((size,)),torch.zeros((size,))
        batch_s_t = torch.stack([torch.Tensor(mini_batch[i]["s_t"]) for i in range(len(mini_batch))])

        for i in range(len(mini_batch)):
            self.qnetwork.train()
            y_hat[i] = self.qnetwork(torch.tensor(list(mini_batch[i]["s_t"])))
            self.qnetwork.eval()
            if not mini_batch[i]["done"]:
                y[i] = (mini_batch[i]["r_t"]
                    + self.gamma * self.qnetwork(torch.tensor([list(i) for i in mini_batch[i]["s_t+1"].values()])).max())

            else:
                y[i] = mini_batch[i]["r_t"]

        self.qnetwork.train()
        loss = torch.nn.MSELoss()(y , y_hat)
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()


    def get_observation_state(self, action = "N"):
        """
            function that constructs the input vector for the qnetwork given an action
            this is done by rotating the initial input vector that is given for the action "N"
            this function is responsible of putting together all the pieces of the rotated bits
            of the new input vector (for more detail read the global action representation of the article)
            params:
            action (str): possible action to perform
            returns (1d-array); input vector of the qnetwork used to predict the
                value of the action given in params in the current state of the agent
        """

        (
            obstacle_vector,
            enemy_vector,
            food_vector
        ) = self.__sensors_to_input__(action)
        previous_action_vector = np.zeros((4,))
        if(self.previous_action is not None):
            for a in range(len(self.action_set)):
                previous_action_vector[a] = (self.action_set[a]
                    == self.previous_action)
            previous_action_vector = self.rotate_previous_action_vector(
                action,
                previous_action_vector
            )
        energy_level_vector = np.array(
            [1 if i==self.energy_level//16 else 0 for i in range(16)])
        if sum(energy_level_vector<1):
            energy_level_vector[15] = 1
        hit_obstacle_vector = np.array([1 if self.hit_obstacle else 0])

        return np.concatenate((
            obstacle_vector,
            enemy_vector,
            food_vector,
            previous_action_vector,
            energy_level_vector,
            hit_obstacle_vector
        ))

    def rotate_previous_action_vector(self, action, vector):
        """
            rotates the previous action with respect to the potential action
            we want to perform
        """
        matrix = np.zeros((3,3))
        if(self.previous_action == "N"):
            matrix[0,1] = 1
        elif(self.previous_action == "W"):
            matrix[1,0] = 1
        elif(self.previous_action == "S"):
            matrix[2,1] = 1
        elif(self.previous_action == "E"):
            matrix[1,2] = 1

        if(action == "W"):
            matrix = np.rot90(np.rot90(np.rot90(matrix)))
        elif(action == "S"):
            matrix = np.rot90(np.rot90(matrix))
        elif(action == "E"):
            matrix = np.rot90(matrix)

        return np.array([matrix[0,1],matrix[1,2],matrix[2,1],matrix[1,0]])

    def __sensors_to_input__(self, rotation_direction):
        """
            function that transforms the current input vector (which is given for the north action)
            to input vectors of other action
        """
        current_obstacle_matrix = self.__sensor_to_matrix__(
            self.obstacle_sensor_list,
            9
        )
        current_enemy_matrix = self.__sensor_to_matrix__(
            self.enemy_sensor_list,
            13
        )
        current_food_matrix = self.__sensor_to_matrix__(
            self.food_sensor_list,
            21
        )
        if(rotation_direction == "N"):
            return (
                self.__matrix_to_vector__(current_obstacle_matrix, "obstacle"),
                self.__matrix_to_vector__(current_enemy_matrix,"enemy"),
                self.__matrix_to_vector__(current_food_matrix, "food")
            )
        if(rotation_direction == "E"):
            return (
                self.__matrix_to_vector__(np.rot90(current_obstacle_matrix), "obstacle"),
                self.__matrix_to_vector__(np.rot90(current_enemy_matrix), "enemy"),
                self.__matrix_to_vector__(np.rot90(current_food_matrix), "food")
            )
        if(rotation_direction == "S"):
            return (
                self.__matrix_to_vector__(
                    np.rot90(np.rot90(current_obstacle_matrix)),"obstacle"
                    ),
                self.__matrix_to_vector__(
                    np.rot90(np.rot90(current_enemy_matrix)),"enemy"
                    ),
                self.__matrix_to_vector__(
                    np.rot90(np.rot90(current_food_matrix)),"food"
                    )
            )
        if(rotation_direction == "W"):
            return (
                self.__matrix_to_vector__(
                    np.rot90(np.rot90(np.rot90(current_obstacle_matrix))),"obstacle"
                    ),
                self.__matrix_to_vector__(
                    np.rot90(np.rot90(np.rot90(current_enemy_matrix))),"enemy"
                    ),
                self.__matrix_to_vector__(
                    np.rot90(np.rot90(np.rot90(current_food_matrix))),"food"
            )
        )

    def __plot_vector_sensor_list__(self):
        """
            function that I used to help me debug and check the output of the sensors and rotations
        """
        def input_to_matrix(input_vec, sensor_type):
            if(sensor_type=="previous_act"):
                mat_size = 3
                matrix = np.zeros((mat_size,mat_size), dtype=int)
                matrix[mat_size//2  ,mat_size//2] = 2.5
                matrix[0,1] = 1
                matrix[1,2] = 1
                matrix[2,1] = 1
                matrix[1,0] = 1
                if(input_vec[0]):
                    matrix[0,1] = 5
                elif(input_vec[1]):
                    matrix[1,2] = 5
                elif(input_vec[2]):
                    matrix[2,1] = 5
                elif(input_vec[3]):
                    matrix[1,0] = 5
                return matrix
            if(sensor_type=="obstacle"):
                sensor_list = self.obstacle_sensor_list
                mat_size = 9
            elif(sensor_type=="enemy"):
                sensor_list = self.enemy_sensor_list
                mat_size = 13
            elif(sensor_type=="food"):
                sensor_list = self.food_sensor_list
                mat_size = 21
            matrix = np.zeros((mat_size,mat_size), dtype=int)
            matrix[mat_size//2  ,mat_size//2] = 2
            for ind in range(len(input_vec)):
                i,j = sensor_list[ind].relative_position
                if(input_vec[ind]):
                    matrix[j+ (mat_size-1)//2  ,i+ (mat_size-1)//2] = 5*input_vec[ind]
                else:
                    matrix[j+ (mat_size-1)//2  ,i+ (mat_size-1)//2] = 1
            return matrix


        for i in range(len(self.action_set)):
            tmp = self.get_observation_state(self.action_set[i])
            obstacles, enemy, food, previous_act = tmp[0:40], tmp[40:72], tmp[72:124], tmp[124:128]
            current_obstacle_matrix = input_to_matrix(obstacles,"obstacle")
            current_enemy_matrix = input_to_matrix(enemy,"enemy")
            current_food_matrix = input_to_matrix(food,"food")
            current_previous_act_matrix = input_to_matrix(previous_act,"previous_act")

            plt.subplot(4,4,i+1)
            sns.heatmap(current_obstacle_matrix, cmap='RdYlGn', vmin=0, vmax=5, linewidth=0.5)
            plt.title(f"Obstacles observés pour l'action {self.action_set[i]}")
            plt.subplot(4,4,4+i+1)
            sns.heatmap(current_enemy_matrix, cmap='RdYlGn', vmin=0, vmax=5, linewidth=0.5 )
            plt.title(f"Enemies observés pour l'action {self.action_set[i]}")
            plt.subplot(4,4,8+i+1)
            sns.heatmap(current_food_matrix, cmap='RdYlGn', vmin=0, vmax=5, linewidth=0.5 )
            plt.title(f"Nourritures observées pour l'action {self.action_set[i]}")
            plt.subplot(4,4,12+i+1)
            sns.heatmap(current_previous_act_matrix, cmap='RdYlGn', vmin=0, vmax=5, linewidth=0.5 )
            plt.title(f"Action precedente observée pour l'action {self.action_set[i]}")
        plt.subplots_adjust(left=0.1,bottom=0.1,right=0.9,top=0.9,wspace=0.4,hspace=0.4)
        plt.suptitle("Vecteur d'observation passé au réseau de neurone afin de predire la valeur de chacune des actions N, W, S, E")
        plt.show()

class Env:
    FOOD_INT = 2
    OBSTACLE_INT = 1
    ENEMY_INT = 4
    AGENT_INT = 3
    EMPTY_INT = 0
    def __init__(self,
        env_size,
        agent,
        initial_enemies_pos,
        obstacles,
        initial_agent_pos,
        learning = True,
        cell_size = 25,
        color_obstacle=(0,0,0),
        color_food = (30,255,30),
        food_radius = 10,
        enemy_color = (255,30,30),
        enemy_radius = 12,
        agent_color = (255,255,30),
        agent_radius = 12,
        color_empty_cell = (255,255,255)
    ):
        """
            class that implements the environment in which the agents evolves
            params:
                env_size((number, number)): width and length of the env (in cells)
                agent (Agent): pointer to the learning agent
                initial_enemies_pos ([(number, number)]): list of grid coord that
                    specify the initial position of the enemies
                learning (bool): wether or not the agent should use this environment to learn or to test
                rest of the params are used to parameterize the rendering of the environment
        """
        self.env_size = env_size
        self.action_set = ["N", "E", "S", "W"]
        self.grid = np.zeros(env_size, dtype=int)
        self.agent = agent
        self.enemies_pos = list(initial_enemies_pos)
        self.obstacles = obstacles
        self.agent_position = initial_agent_pos
        possible_position = [
            (i,j) for i in range(25) for j in range(25)
                if (i,j) not in obstacles
                and (i,j) not in initial_agent_pos
                and (i,j) not in initial_enemies_pos
        ]
        self.food =[
            possible_position[i]
            for i in np.random.choice(range(len(possible_position)), size=15)
        ]
        self.grid[initial_agent_pos[0], initial_agent_pos[1]] = Env.AGENT_INT
        for o in self.obstacles:
            i,j = o
            self.grid[i,j] = Env.OBSTACLE_INT
        for f in self.food:
            i,j = f
            self.grid[i,j] = Env.FOOD_INT
        for e in self.enemies_pos:
            i,j = e
            self.grid[i,j] = Env.ENEMY_INT
        self.over = False
        self.learning = learning
        self.cell_size = cell_size
        self.color_obstacle = color_obstacle
        self.color_food = color_food
        self.food_radius = food_radius
        self.color_empty_cell = color_empty_cell
        self.enemy_color = enemy_color
        self.food_stack = []
        self.remaining_food = 15
        self.enemy_radius = enemy_radius
        self.agent_color = agent_color
        self.agent_radius = agent_radius
        self.agent.action_set = self.action_set

    def __regenerate_food__(self):
        """
            function responsible of regenerating the food that the enemies step on
        """
        to_delete = []
        for i in range(len(self.food_stack)):
            if(self.grid[self.food_stack[i][0],self.food_stack[i][1]] !=  Env.ENEMY_INT):
                self.grid[self.food_stack[i][0],self.food_stack[i][1]] = Env.FOOD_INT
                to_delete.append(i)
        deleted = 0
        for i in to_delete:
            del self.food_stack[i-deleted]
            deleted += 1

    def __move_enemy__(self, enemy_ind):
        """
            function that moves the enemies following the algorithm described in the article
        """
        def T(dist):
            if(dist <=4):
                return 15 - dist
            elif(dist <=15):
                return 9 - dist/2
            else:
                return 1

        def W(angle):
            return (180 - abs(angle))/180

        proba = np.random.rand()
        if(proba>0.2):
            enemy_pos = self.enemies_pos[enemy_ind]
            agent_pos = self.agent_position
            action_probas = np.zeros(len(self.action_set))
            pi = np.zeros(len(self.action_set))
            angles = np.zeros(len(self.action_set))
            #if(self.grid[agent_pos[0]-1, agent_pos[1]] == 1):

            delta_x = agent_pos[0] - enemy_pos[0]
            delta_y = agent_pos[1] - enemy_pos[1]
            dist = abs(delta_x) + abs(delta_y)

            #compute the angles between each action vector and
            #the position of the agent
            if(delta_x > 0):
                if(delta_y > 0):
                    #east
                    angles[1] = np.arctan(abs(delta_y)/abs(delta_x))
                    #south
                    angles[2] = np.pi/2 - angles[1]
                    #west
                    angles[3] = np.pi - angles[1]
                    #north
                    angles[0] = np.pi - angles[2]
                elif(delta_y == 0):
                    angles[1] = np.pi/2
                    angles[3] = np.pi/2
                    angles[0] = np.pi
                    angles[2] = 0
                else:
                    #east
                    angles[3] = np.arctan(abs(delta_y)/abs(delta_x))
                    #south
                    angles[2] = np.pi/2 - angles[3]
                    #north
                    angles[0] = np.pi - angles[2]
                    #west
                    angles[1] = np.pi - angles[3]

            elif(delta_x == 0):
                if(delta_y > 0):
                    angles[1] = 0
                    angles[3] = np.pi
                    angles[0] = np.pi/2
                    angles[2] = np.pi/2
                elif(delta_y == 0):
                    self.over = True
                    return self.over

                else:
                    angles[1] = np.pi
                    angles[3] = 0
                    angles[0] = np.pi/2
                    angles[2] = np.pi/2
            else:
                if(delta_y > 0):
                    #west
                    angles[1] = np.arctan(abs(delta_y)/abs(delta_x))
                    #north
                    angles[0] = np.pi/2 - angles[1]
                    #east
                    angles[3] = np.pi - angles[1]
                    #south
                    angles[2] = np.pi - angles[0]
                elif(delta_y == 0):
                    angles[1] = np.pi/2
                    angles[3] = np.pi/2
                    angles[0] = 0
                    angles[2] = np.pi
                else:
                    #east
                    angles[3] = np.arctan(abs(delta_y)/abs(delta_x))
                    #north
                    angles[0] = np.pi/2 - angles[3]
                    #south
                    angles[2] = np.pi - angles[0]
                    #west
                    angles[1] = np.pi - angles[3]
            #proba for north action
            if(enemy_pos[1] > 0 and
                (self.grid[enemy_pos[0], enemy_pos[1] - 1]
                not in [Env.OBSTACLE_INT, Env.ENEMY_INT])):
                pi[0] = np.exp(0.33 * W(angles[0]*180) * T(dist))
            #proba for west action
            if(enemy_pos[0] > 0 and
                (self.grid[enemy_pos[0]-1, enemy_pos[1]]
                not in [Env.OBSTACLE_INT, Env.ENEMY_INT])):
                pi[3] = np.exp(0.33 * W(angles[1]*180) * T(dist))
            #proba for south action
            if(enemy_pos[1] < self.env_size[1]-1 and
                (self.grid[enemy_pos[0], enemy_pos[1]+1]
                not in [Env.OBSTACLE_INT, Env.ENEMY_INT])):
                pi[2] = np.exp(0.33 * W(angles[2]*180) * T(dist))
            #proba for east action
            if(enemy_pos[0] < self.env_size[0]-1 and
                (self.grid[enemy_pos[0]+1, enemy_pos[1]]
                not in [Env.OBSTACLE_INT, Env.ENEMY_INT])):
                pi[1] = np.exp(0.33 * W(angles[3]*180) * T(dist))
            if(sum(pi)):
                for i in range(len(action_probas)):
                    action_probas[i] = pi[i] / sum(pi)

                action_to_execute = np.random.choice(self.action_set, p = action_probas)
                if(action_to_execute == "N"
                    and enemy_pos[1]-1 in range(self.env_size[1])):
                    self.grid[
                        self.enemies_pos[enemy_ind][0],
                        self.enemies_pos[enemy_ind][1]
                    ] = Env.EMPTY_INT
                    self.enemies_pos[enemy_ind] = (enemy_pos[0], enemy_pos[1]-1)
                elif(action_to_execute == "W"
                    and enemy_pos[0]-1 in range(self.env_size[0])):
                    self.grid[
                        self.enemies_pos[enemy_ind][0],
                        self.enemies_pos[enemy_ind][1]
                    ] = Env.EMPTY_INT
                    self.enemies_pos[enemy_ind] = (enemy_pos[0]-1, enemy_pos[1])

                if(action_to_execute == "S"
                    and enemy_pos[1]+1 in range(self.env_size[1])):
                    self.grid[
                        self.enemies_pos[enemy_ind][0],
                        self.enemies_pos[enemy_ind][1]
                    ] = Env.EMPTY_INT
                    self.enemies_pos[enemy_ind] = (enemy_pos[0], enemy_pos[1]+1)

                elif(action_to_execute == "E"
                    and enemy_pos[0]+1 in range(self.env_size[0])):
                    self.grid[
                        self.enemies_pos[enemy_ind][0],
                        self.enemies_pos[enemy_ind][1]
                    ] = Env.EMPTY_INT
                    self.enemies_pos[enemy_ind] = (enemy_pos[0]+1, enemy_pos[1])

            if(self.grid[
                self.enemies_pos[enemy_ind][0],
                self.enemies_pos[enemy_ind][1]
            ] == Env.FOOD_INT):
                self.food_stack.append(
                    (self.enemies_pos[enemy_ind][0],self.enemies_pos[enemy_ind][1])
                )
            elif(self.grid[
                self.enemies_pos[enemy_ind][0],
                self.enemies_pos[enemy_ind][1]
            ] == Env.AGENT_INT):
                self.over = True
            self.grid[
                self.enemies_pos[enemy_ind][0],
                self.enemies_pos[enemy_ind][1]
            ] = Env.ENEMY_INT
            return self.over


    def play_env(self,learning_algo ,T=1/20, render = False):

        def drawGrid(screen,rectangles, draw_sensors = False, sensor_type=None):
            screen.fill((150,150,150))
            text = font.render(f'Energy: {self.agent.energy_level}', True, (0,0,0))
            textRect = text.get_rect()
            textRect.center = (
                (self.cell_size*self.env_size[0]+OFFSET) // 2 ,
                self.cell_size*self.env_size[1]+ OFFSET//1.25
            )
            screen.blit(text, textRect)
            for i in range(self.env_size[0]):
                for rect, color, circle_color, circle_radius in rectangles[i]:
                    pg.draw.rect(screen, color, rect)
                    if(circle_color):
                        pg.draw.circle(
                            screen,
                            circle_color,
                            (rect.centerx, rect.centery),
                            circle_radius
                        )
            if(draw_sensors):
                if(sensor_type is None):
                    raise Exception("Please provide sensor type")
                else:
                    if(sensor_type == "obstacle"):
                        sensor_list = self.agent.obstacle_sensor_list
                    elif(sensor_type == "food"):
                        sensor_list = self.agent.food_sensor_list
                    elif(sensor_type == "enemy"):
                        sensor_list = self.agent.enemy_sensor_list
                    for s in sensor_list:
                        i = self.agent_position[0]+s.relative_position[0]
                        j = self.agent_position[1] + s.relative_position[1]
                        if(i in range(self.env_size[0]) and j in range(self.env_size[1])):
                            r = rectangles[i][j][0]

                            if(s.activated):
                                color = (255,0,255)
                            else:
                                color = (128,128,128)
                            pg.draw.circle(
                                screen,
                                color,
                                (
                                    r.centerx,
                                    r.centery
                                ),
                                4
                            )
            pg.display.flip()

        def update_grid(OFFSET):
            rectangles = [[] for i in range(self.env_size[0])]
            for x in range(self.env_size[0]):
                for y in range(self.env_size[1]):
                    rect = pg.Rect(
                        x * (self.cell_size+1)+ OFFSET/2,
                        y * (self.cell_size+1)+ OFFSET/2,
                        self.cell_size,
                        self.cell_size
                    )
                    #draw obstacle
                    if(self.grid[x,y]==Env.OBSTACLE_INT):
                        rectangles[x].append((rect, self.color_obstacle, None, None))
                    #draw food
                    elif(self.grid[x,y] == Env.FOOD_INT):
                        rectangles[x].append((
                            rect,
                            self.color_empty_cell,
                            self.color_food,
                            self.food_radius
                        ))
                    #draw agent
                    elif(self.grid[x,y] == Env.AGENT_INT):
                        rectangles[x].append((
                            rect,
                            self.color_empty_cell,
                            self.agent_color,
                            self.agent_radius
                        ))
                    #draw enemies
                    elif(self.grid[x,y] == Env.ENEMY_INT):
                        rectangles[x].append((
                            rect,
                            self.color_empty_cell,
                            self.enemy_color,
                            self.enemy_radius
                        ))
                    #draw empty grid squares
                    else:
                        rectangles[x].append((
                            rect,
                            self.color_empty_cell,
                            None,
                            None
                        ))

            return rectangles

        if(render):
            pg.init()
            OFFSET = 150
            screen = pg.display.set_mode(
                (
                    self.cell_size*self.env_size[0]+OFFSET,
                    self.cell_size*self.env_size[1]+OFFSET
                )
            )
            font = pg.font.SysFont("comicsans", 30, True)

            rectangles = update_grid(OFFSET)
            clock = pg.time.Clock()
        self.update_agent_sensors()

        while not self.over:

            action = self.agent.select_action(T)
            s_t = self.agent.get_observation_state(action)
            r = self.perform_agent_action(action)
            for e in range(len(self.enemies_pos)):
                if self.__move_enemy__(e):
                    r = -1
                    break

            self.__regenerate_food__()

            self.update_agent_sensors()

            if(self.learning):
                if(r < 0):
                    self.agent.memory.append({
                        "s_t":s_t,
                        "r_t":r,
                        "s_t+1":{},
                        "done":True
                    })
                else:
                    self.agent.memory.append({
                        "s_t":s_t,
                        "r_t":r,
                        "s_t+1":{
                            i:self.agent.get_observation_state(i)
                            for i in self.agent.action_set
                        },
                        "done":False
                    })
                if(learning_algo == "DQN"):
                    self.agent.DQNlearn()
                if(learning_algo == "QCON"):
                    self.agent.qconlearn()
            if(render):
                rectangles = update_grid(OFFSET)
                drawGrid(screen, rectangles)
                pg.display.flip()
                clock.tick(20)
        return self.remaining_food


    def perform_agent_action(self,action):
        """
            function that performs an action of the learning agent
            params:
                action (str): action to perform
            returns (number): reward of doing the action
        """
        r = 0
        self.agent.previous_action = action
        i,j = self.agent_position
        if action == "W":
            if i-1 in range(25) and self.grid[i-1,j] != Env.OBSTACLE_INT:
                self.agent_position = (i-1,j)
                self.agent.hit_obstacle = False
            else:
                self.agent.hit_obstacle = True
        elif action == "N":
            if j-1 in range(25) and self.grid[i,j-1] != Env.OBSTACLE_INT:
                self.agent_position = (i,j-1)
                self.agent.hit_obstacle = False
            else:
                self.agent.hit_obstacle = True
        elif action == "E":
            if i+1 in range(25) and self.grid[i+1,j] != Env.OBSTACLE_INT:
                self.agent_position = (i+1,j)
                self.agent.hit_obstacle = False
            else:
                self.agent.hit_obstacle = True
        elif action == "S":
            if j+1 in range(25) and self.grid[i,j+1] != Env.OBSTACLE_INT:
                self.agent_position = (i,j+1)
                self.agent.hit_obstacle = False
            else:
                self.agent.hit_obstacle = True

        self.agent.energy_level -= 1
        if self.grid[self.agent_position[0],self.agent_position[1]] ==  Env.FOOD_INT:
            self.agent.energy_level += 15
            r = 0.4
            self.remaining_food -=1
        if((
            self.agent.energy_level <= 0 or
            self.grid[self.agent_position[0],self.agent_position[1]] ==  Env.ENEMY_INT
            )):
                self.over = True
                r = -1
        self.grid[i,j] = Env.EMPTY_INT
        self.grid[self.agent_position[0],self.agent_position[1]] = Env.AGENT_INT
        return r

    def __update_anget_sensor__(self, s):
        done = False
        for cell in s.activation_cells:
            i = self.agent_position[0] + s.relative_position[0] + cell[0]
            j = self.agent_position[1] + s.relative_position[1] + cell[1]
            if(i>=0 and i<self.env_size[0] and j>=0 and j<self.env_size[1]):
                if(self.grid[i,j] == s.activation_value):
                    done = True
                    s.activated = True
                    break
        if(not done):
            s.activated = False


    def update_agent_sensors(self):
        #update obstacle sensors
        for s in (self.agent.obstacle_sensor_list
            + self.agent.food_sensor_list
            + self.agent.enemy_sensor_list):
            self.__update_anget_sensor__(s)
