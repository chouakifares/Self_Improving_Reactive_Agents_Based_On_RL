from classes import *
from Env_Initial_settings import *
from Agent_Sensors import *
import csv
from tqdm import tqdm


learning_algo = "QCON"
replay = True
mini_batch_size = 16
a = Agent(OBSTACLE_SENSORS, ENEMY_SENSORS, FOOD_SENSORS, replay=replay, mini_batch_size = mini_batch_size)


T = 20
all_rs = []
for i in tqdm(range(300)):
    for j in range(20):
        e = Env(ENV_SIZE, a, INITIAL_ENEMIES_POS, OBSTACLES, INITIAL_AGENT_POS)
        a.energy_level = 40
        a.previous_action = None
        a.hit_obstacle = False
        e.play_env(render=False,learning_algo=learning_algo ,T = 1/T)

    r = []
    for j in range(50):
        e = Env(ENV_SIZE, a, INITIAL_ENEMIES_POS, OBSTACLES, INITIAL_AGENT_POS, learning = False)
        a.energy_level = 40
        a.previous_action = None
        a.hit_obstacle = False
        r.append(e.play_env(learning_algo = None,T = 0,render = False))
    all_rs.append(r)
    T+=1

with open(f'run_{learning_algo}_{f"replay_{mini_batch_size}" if replay else "noreplay"}.csv', 'w', newline='') as f:
    # Create a CSV writer
    writer = csv.writer(f)

    # Write the data to the file
    for row in all_rs:
        writer.writerow(row)
